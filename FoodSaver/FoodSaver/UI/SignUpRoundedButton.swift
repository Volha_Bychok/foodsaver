//
//  SignUpRoundedButton.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 14/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class SignUpRoundedButton: RoundedButton {

    override open var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor(red:0.13, green:0.38, blue:0.54, alpha:1.0) : UIColor(red:0.27, green:0.57, blue:0.69, alpha:1.0)
            if isSelected {
                setTitle(L.o.c("Join now"), for: .normal)
            } else {
                setTitle(L.o.c("SignUp"), for: .normal)
            }
        }
    }

}
