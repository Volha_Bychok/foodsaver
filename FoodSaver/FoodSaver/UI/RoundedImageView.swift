//
//  RoundedImageView.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 19/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }
}
