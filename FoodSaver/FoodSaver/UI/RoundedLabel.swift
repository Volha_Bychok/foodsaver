//
//  RoundedLabel.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 10/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class RoundedLabel: UILabel {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 10
    }
}
