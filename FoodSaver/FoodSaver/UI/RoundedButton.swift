//
//  RoundedButton.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/15/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import GoogleSignIn

class RoundedButton: UIButton {
    
    override open var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor(red:0.13, green:0.38, blue:0.54, alpha:1.0) : UIColor(red:0.27, green:0.57, blue:0.69, alpha:1.0)
        }
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 10
}

}
