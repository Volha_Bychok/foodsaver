//
//  RoundedView.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 19/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class RoundedView: UIView {
    
    override func layoutSubviews() {
        layer.cornerRadius = frame.height / 20
        let customDarkBlueColor = UIColor(red:0.13, green:0.38, blue:0.54, alpha:1.0).cgColor
        layer.borderColor = customDarkBlueColor
        layer.borderWidth = 1
        layer.shadowColor = customDarkBlueColor
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowOpacity = 1
        layer.shadowRadius = 3
        
    }}
