//
//  ContactButton.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 09/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class ContactButton: UIButton {

    override open var isUserInteractionEnabled: Bool {
        didSet {
            isSelected = isUserInteractionEnabled
        }
    }
    
}
