//
//  ProjectAlerts.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 25/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import UIKit

class ProjectAlerts {
    
    static let shared = ProjectAlerts()
    
    private init() {
    }
    
    func presentLoginAlert(page: UIViewController) {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = page.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        page.view.addSubview(blurEffectView)
        let alertController = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message: NSLocalizedString("To load this page you have to login to your account", comment: ""), preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: NSLocalizedString("Back to list", comment: ""), style: .cancel, handler: {_ in
            page.tabBarController?.selectedIndex = 0
        })
        let loginAction = UIAlertAction(title: NSLocalizedString("Login/ Register", comment: ""), style: .default, handler: {_ in
            Navigation.shared.goToLoginPage()
        })
        alertController.addAction(cancelAction)
        alertController.addAction(loginAction)
        page.present(alertController, animated: true, completion: nil)
    }
    
    func presentAlert(title: String, message: String, on page: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        page.present(alertController, animated: true, completion: nil)
    }
    
    func presentPleaseWaitAlert(on page: UIViewController) {
        let waitingAlert = UIAlertController(title: nil, message: NSLocalizedString("Post is being saved\n\n", comment: ""), preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(style: .whiteLarge)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        waitingAlert.view.addSubview(spinnerIndicator)
        page.present(waitingAlert, animated: false, completion: nil)
    }

}

