//
//  Navigation.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/24/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import UIKit

class Navigation {
    
    static let shared = Navigation()
    
    private init() {
    }
    
    func goToList() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = mainStoryboard.instantiateInitialViewController() as? UITabBarController
        appDelegate.window?.rootViewController = rootVC
    }
    
    func goToLoginPage() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let loginStoryboard = UIStoryboard(name: "Login", bundle: nil)
        if let rootVC = loginStoryboard.instantiateInitialViewController() {
            appDelegate.window?.rootViewController?.present(rootVC, animated: true, completion: nil)
        }
    }
    
}
