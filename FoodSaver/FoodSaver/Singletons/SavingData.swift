//
//  SavingUserData.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 17/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class SavingData {
    
    static let shared = SavingData()
    
    private init() {
    }
    
    func saveData(dictionary: [String:Any], ID: String, folder: String) {
        let value = [ID: dictionary]
        Database.database().reference().child(folder).updateChildValues(value, withCompletionBlock: {error, _ in
            if let error = error {
                NotificationCenter.default.post(name: NSNotification.Name("DataSavingStatus"), object: nil, userInfo: ["status": error.localizedDescription])
                return
            }
            NotificationCenter.default.post(name: NSNotification.Name("DataSavingStatus"), object: nil, userInfo: ["status": "saved"])
        })
    }
    
    func saveImageToFolder(_ folderName: String, image: UIImage?, id: String) {
        let imagesReference = Storage.storage().reference().child(folderName)
        guard let data = image?.jpegData(compressionQuality: 0)
            else {return}
        let spaceReference = imagesReference.child(id)
        spaceReference.putData(data, metadata: nil) { (metadata, error) in
            if let error = error {
                NotificationCenter.default.post(name: NSNotification.Name("DataSavingStatus"), object: nil, userInfo: ["status": error.localizedDescription])
                return
            }
        }
    }
    
}
