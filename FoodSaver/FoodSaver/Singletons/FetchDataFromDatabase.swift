//
//  FetchDataFromDatabase.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 31/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

struct FetchDataFromDatabase {
    
    static let shared = FetchDataFromDatabase()
    
    private init() {
    }
    
    func fetchingUserData(ID: String, andUse: @escaping (UserProfile) -> Void) {
        Database.database().reference().child("users").child(ID).observeSingleEvent(of: .value, with: {(snapshot) in
            guard let userData = snapshot.value as? [String:Any]
                else {return}
            let user = UserProfile(id: ID, dictionary: userData)
            andUse(user)
        }) {(error) in
            print(error)
        }
    }
    
    func fetchDataFromDatabase(andUseFor: @escaping ([Post]) -> Void) {
        var posts = [Post]()
        Database.database().reference().child("posts").observeSingleEvent(of: .value, with: { (snapshot) in
            guard let value = snapshot.value as? [String:Any] else {return}
            for (_, value) in value {
                guard let value = value as? [String: Any],
                    let post = Post(dictionary: value)
                    else {return}
                if post.expirationDate <= Date() || post.status.rawValue == 0 {
                    Database.database().reference().child("posts").child(post.postID).removeValue()
                    Storage.storage().reference().child("postPhotos").child(post.postID).delete { error in
                        if let error = error {
                            print(error)
                        }
                    }
                } else {
                    posts.append(post)
                }
            }
            andUseFor(posts)
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    
}
