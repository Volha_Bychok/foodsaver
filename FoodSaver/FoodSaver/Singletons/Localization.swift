//
//  Localization.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 13/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation

class L {
    static let o = L()
    private init(){
    }
    func c(_ string: String) -> String {
        return NSLocalizedString(string, comment: "")
    }
}
