//
//  Login.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 11/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import FirebaseDatabase

class Login {
    
    static let shared = Login()
    private var provider = OAuthProvider(providerID: "twitter.com")
    
    private init() {
    }
    
    func facebookLogin(on vc: UIViewController) {
        LoginManager().logIn(permissions: ["email", "public_profile"], from: vc, handler: { [weak vc] (result, error) in
            guard let vc = vc else {return}
            if error == nil {
                GraphRequest(graphPath: "me", parameters: ["fields": "email, name"], tokenString: AccessToken.current?.tokenString, version: nil, httpMethod: HTTPMethod(rawValue: "GET")).start(completionHandler: { (nil, result, error) in
                    if error == nil {
                        let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
                        Auth.auth().signIn(with: credential, completion: { (result, error) in
                            if error == nil {
                                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                                vc.view.endEditing(true)
                                Navigation.shared.goToList()
                                guard let newUser = result?.additionalUserInfo?.isNewUser else {return}
                                if newUser {
                                    guard let userData = Auth.auth().currentUser,
                                        let name = userData.displayName,
                                        let uid = Auth.auth().currentUser?.uid,
                                        let photo = userData.photoURL
                                        else {return}
                                    let user = UserProfile(id: uid, email: userData.email, nickname: name, photoURL: photo)
                                    user.saveUserProfileToDatabase()
                                }
                            } else {
                                ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: String(describing: error), on: vc)
                            }
                        })
                    } else {
                        print(error!.localizedDescription)
                    }
                })
            } else {
                print(error!.localizedDescription)
            }
        })
    }
    
    
    func twitterLogin(on vc: UIViewController?) {
        provider.getCredentialWith(nil) { [weak vc] credential, error in
            guard let vc = vc else {return}
            if error != nil {
                print(error!.localizedDescription)
            }
            if credential != nil {
                Auth.auth().signIn(with: credential!) { authResult, error in
                    if error != nil {
                        ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: error!.localizedDescription, on: vc)
                    }
                    UserDefaults.standard.set(true, forKey: "isLoggedIn")
                    vc.view.endEditing(true)
                    Navigation.shared.goToList()
                    guard let newUser = authResult?.additionalUserInfo?.isNewUser else {return}
                    if newUser {
                        guard let profileInfo = authResult?.additionalUserInfo?.profile,
                            let name = profileInfo["name"] as? String,
                            let uid = Auth.auth().currentUser?.uid,
                            let photo = profileInfo["profile_image_url"] as? String
                            else {return}
                        let photoURL = URL(string: photo)
                        let user = UserProfile(id: uid, email: nil, nickname: name, photoURL: photoURL)
                        user.saveUserProfileToDatabase()
                    }
                }
            }
        }
    }
    
    func loginWithEmail(on vc: UIViewController?, with email: String?, password: String?) {
        guard let email = email,
            let password = password
            else {return}
        Auth.auth().signIn(withEmail: email, password: password) { [weak vc] (user, error) in
            guard let vc = vc else {return}
            if error == nil {
                guard let currentUser = Auth.auth().currentUser else {return}
                if currentUser.isEmailVerified {
                    UserDefaults.standard.set(true, forKey: "isLoggedIn")
                    vc.view.endEditing(true)
                    Navigation.shared.goToList()
                } else {
                    ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: NSLocalizedString("Please confirm your email address", comment: ""), on: vc)
                }
            } else {
                ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: error!.localizedDescription, on: vc)
            }
        }
    }
    
    func signUp(on vc: LoginViewController?, with email: String?, password: String?, nickname: String?) {
        guard let email = email,
            let password = password,
            let nickname = nickname
            else {return}
        Auth.auth().createUser(withEmail: email, password: password){ [weak vc] (user, error) in
            guard let vc = vc else {return}
            if error == nil {
                Auth.auth().currentUser?.sendEmailVerification(completion: nil)
                guard let uid = Auth.auth().currentUser?.uid else {return}
                let user = UserProfile(id: uid, email: email, nickname: nickname, photoURL: nil)
                user.saveUserProfileToDatabase()
                ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Account is created", comment: ""), message: NSLocalizedString("Please confirm your email address", comment: ""), on: vc)
                vc.configurePageState(login: true)
            } else {
                ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: error!.localizedDescription, on: vc)
            }
        }
    }
    
    
}
