//
//  AddViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/16/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseAuth
import FirebaseStorage

class AddViewController: ScrollViewController {
    
    //MARK: Properties
    
    private var location: Location?
    private var user: UserProfile?
    
    // MARK: Outlets
    
    @IBOutlet weak var uploadPhotoLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var expirationTimeLabel: UILabel!
    @IBOutlet weak var submitButton: RoundedButton!
    
    
    @IBOutlet weak var expirationDateInfoLabel: UILabel!
    @IBOutlet weak var addressInfoLabel: UILabel!
    @IBOutlet weak var descriptionInfoLabel: UILabel!
    @IBOutlet weak var photoInfoLabel: UILabel!
    @IBOutlet weak var addressWarningLabel: UILabel!
    
    @IBOutlet weak var uploadPhotoButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var openCameraButton: UIButton!
    @IBOutlet weak var closeImageButton: UIButton!
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var saveAddressToProfileButton: UIButton!
    @IBOutlet weak var useAddressFromProfileButton: UIButton!
    
    // MARK:  Buttons
    
    @IBAction func openDescriptionInfoButton(_ sender: UIButton) {
        descriptionInfoLabel.isHidden = !descriptionInfoLabel.isHidden
    }
    
    @IBAction func openAddressInfoButton(_ sender: UIButton) {
        addressInfoLabel.isHidden = !addressInfoLabel.isHidden
    }
    
    @IBAction func expirationDateInfoButton(_ sender: Any) {
        expirationDateInfoLabel.isHidden = !expirationDateInfoLabel.isHidden
    }
    
    @IBAction func photoInfoButton(_ sender: UIButton) {
        photoInfoLabel.isHidden = !photoInfoLabel.isHidden
    }
    
    
    @IBAction func saveAddressToProfileButton(_ sender: UIButton) {
        guard let user = user else {return}
        if addressTextView.text != NSLocalizedString("city, street/ metro station, house №", comment: "") && addressWarningLabel.isHidden {
            let address = addressTextView.text
            let user = UserProfile(id: user.id, email: user.email, nickname: user.nickname, photo: nil, rating: user.rating, address: address, phoneNumber: user.phoneNumber, viber: user.viber, telegram: user.telegram)
            user.saveUserProfileToDatabase()
        }
    }
    
    @IBAction func fetchAddressFromProfileButton(_ sender: UIButton) {
        guard let user = user else {return}
        if user.address == nil {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: NSLocalizedString("There is no address in your profile", comment: ""), on: self)
        } else {
            addressTextView.textColor = .black
            addressTextView.text = user.address
        }
    }
    
    @IBAction func closeImageButton(_ sender: UIButton) {
        imageView.image = nil
        imageViewIsHidden(true)
    }
    
    @IBAction func cameraButtonTapped(_ sender: Any) {
        presentImagePickerController(sourceType: .camera)
    }
    
    @IBAction func uploadPhotoButtonTapped(_ sender: Any) {
        presentImagePickerController(sourceType: .photoLibrary)
    }
    
    @IBAction func submitButtonTapped(_ sender: RoundedButton) {
        if imageView.image == UIImage(named: "placeholder") {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Photo is not added", comment: ""), message: NSLocalizedString("Please add a photo to your post", comment: ""), on: self)
            return
        } else if descriptionTextView.text == NSLocalizedString("max 180 symbols", comment: "") {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Description is not added", comment: ""), message: NSLocalizedString("Please add a description to your post", comment: ""), on: self)
            return
        } else if !addressWarningLabel.isHidden {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Address does not exist", comment: ""), message: NSLocalizedString("Please specify the address", comment: ""), on: self)
            return
        } else if datePicker.date <= Date() {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Expiration time is in the past", comment: ""), message: NSLocalizedString("Please choose date in future", comment: ""), on: self)
            return
        } else if user?.email == nil && user?.phoneNumber == nil {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("It's impossible to contact you", comment: ""), message: NSLocalizedString("Please add a email or phone number to your profile", comment: ""), on: self)
        } else {
            savePost()
        }
    }
    
    // MARK: methods for submitButton
    
    func savePost() {
        let today = Date()
        let postID = UUID().uuidString
        guard let userID = Auth.auth().currentUser?.uid,
            let location = location,
            let image = imageView.image
            else {return}
        let post = Post(postID: postID, userID: userID, photo: image, description: descriptionTextView.text, address: location, expirationDate: datePicker.date, rating: 0, status: .active, likedBy: [" ":" "], creationDate: today)
        post?.savePostToDatabase()
        ProjectAlerts.shared.presentPleaseWaitAlert(on: self)
    }
    
    override func clearForm() {
        imageView.image = UIImage(named: "placeholder")
        descriptionTextView.text = ""
        addressTextView.text = ""
        datePicker.date = Date()
        textViewFinishedBeenEditing()
    }
    
    // MARK: Managing placeholder in textView
    
    private func addDescriptionTextViewNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(textViewStartedBeenEditing), name: UITextView.textDidBeginEditingNotification, object: descriptionTextView)
        NotificationCenter.default.addObserver(self, selector: #selector(textViewFinishedBeenEditing), name: UITextView.textDidEndEditingNotification, object: descriptionTextView)
    }
    
    private func addAddressTextViewNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(textViewStartedBeenEditing), name: UITextView.textDidBeginEditingNotification, object: addressTextView)
        NotificationCenter.default.addObserver(self, selector: #selector(textViewFinishedBeenEditing), name: UITextView.textDidEndEditingNotification, object: addressTextView)
    }
    
    @objc func textViewStartedBeenEditing(notification: NSNotification) {
        let view = notification.object as? UITextView
        if view?.text == NSLocalizedString("max 180 symbols", comment: "") || view?.text == NSLocalizedString("city, street/ metro station, house №", comment: "") {
            view?.textColor = UIColor.black
            view?.text = ""
            addressWarningLabel.isHidden = true
        }
    }
    
    @objc func textViewFinishedBeenEditing() {
        if descriptionTextView.text == "" {
            descriptionTextView.textColor = UIColor.lightGray
            descriptionTextView.text = NSLocalizedString("max 180 symbols", comment: "")
        } else if addressTextView.text == "" {
            addressTextView.textColor = UIColor.lightGray
            addressTextView.text = NSLocalizedString("city, street/ metro station, house №", comment: "")
        } else if addressTextView.text != NSLocalizedString("city, street/ metro station, house №", comment: "") {
            turnAddressIntoLocation(address: addressTextView.text)
        }
    }
    
    // MARK: Adding text to the Navigation Item Title
    
    private func setNavigationBarSettings() {
        guard let lightBarStyle = UIBarStyle(rawValue: 1) else {return}
        navigationController?.navigationBar.barStyle = lightBarStyle
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44.0))
        navigationController?.navigationBar.barTintColor = UIColor(red:0.13, green:0.38, blue:0.54, alpha:1.0)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.text = NSLocalizedString("Please fill in the form to add a new post\n(all the lines are necessarily)", comment: "")
        label.textColor = UIColor(red:0.84, green:0.84, blue:0.84, alpha:1.0)
        self.navigationItem.titleView = label
    }
    
    // MARK: Location
    
    private func turnAddressIntoLocation(address: String) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) {
            placemarks, error in
            let placemark = placemarks?.first
            guard let lat = placemark?.location?.coordinate.latitude,
                let lon = placemark?.location?.coordinate.longitude
                else {
                    self.addressWarningLabel.isHidden = false
                    return
            }
            self.location = Location(title: address, coordinates: CLLocation(latitude: lat, longitude: lon))
            self.addressWarningLabel.isHidden = true
        }
    }
    
    // Set initial UI settings
    
    private func imageViewIsHidden(_ sign: Bool) {
        imageView.isHidden = sign
        closeImageButton.isHidden = sign
    }
    
    private func hideInfoLabels() {
        expirationDateInfoLabel.isHidden = true
        addressInfoLabel.isHidden = true
        descriptionInfoLabel.isHidden = true
        photoInfoLabel.isHidden = true
    }
    
    private func setLabelsText() {
        uploadPhotoLabel.text = L.o.c("Upload photo")
        descriptionLabel.text = L.o.c("description")
        addressLabel.text = L.o.c("address")
        expirationTimeLabel.text = L.o.c("expiration time")
        submitButton.setTitle(L.o.c("Submit"), for: .normal)
        descriptionInfoLabel.text = L.o.c("describe product, specify if you can delivery it by youself")
        descriptionTextView.text = L.o.c("max 180 symbols")
        addressTextView.text = L.o.c("city, street/ metro station, house №")
        addressInfoLabel.text = L.o.c("specify the address where product is waiting for ")
        photoInfoLabel.text = L.o.c("you can't submit post without photo")
        addressWarningLabel.text = L.o.c("address doesn't exist")
        saveAddressToProfileButton.setTitle(L.o.c("save adress to my profile"), for: .normal)
        useAddressFromProfileButton.setTitle(L.o.c("use addres from my profile"), for: .normal)
        expirationDateInfoLabel.text = L.o.c("time when your post will dissappear from list")
    }
    
    // Save user data to property
    
    private func saveUser(user: UserProfile) {
        self.user = user
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLabelsText()
            hideInfoLabels()
            imageViewIsHidden(true)
            addressWarningLabel.isHidden = true
            setNavigationBarSettings()
            descriptionTextView.delegate = self
            addDescriptionTextViewNotificationObserver()
            addAddressTextViewNotificationObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !UserDefaults.standard.bool(forKey: "isLoggedIn") {
            ProjectAlerts.shared.presentLoginAlert(page: self)
        } else {
            guard let currentUser = Auth.auth().currentUser else {return}
            FetchDataFromDatabase.shared.fetchingUserData(ID: currentUser.uid, andUse: saveUser)
        }
    }
    
}

// MARK: UIImagePickerControllerDelegate

extension AddViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private func presentImagePickerController(sourceType: UIImagePickerController.SourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = sourceType
        present(imagePickerController, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            imageView.image = image
            imageViewIsHidden(false)
        }
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: UITextViewDelegate

extension AddViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        return updatedText.count <= 180
    }
    
}







