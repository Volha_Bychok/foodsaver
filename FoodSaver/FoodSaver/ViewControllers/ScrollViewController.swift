//
//  ScrollViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 07/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class ScrollViewController: UIViewController, UIScrollViewDelegate {
    
     @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: Managing keyboard behavior
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // MARK: checking if post was succesfully saved
    
    @objc private func statusOfPostSaving(notification: NSNotification) {
        let userInfo = notification.userInfo as? [String: Any]
        let status = userInfo?["status"] as? String
        self.dismiss(animated: true, completion: nil)
        if status == "saved" {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Thank you!", comment: ""), message: NSLocalizedString("Data was saved", comment: ""), on: self)
            clearForm()
        } else {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong. Please try again", comment: ""), message: status ?? "", on: self)
        }
    }
    
    func clearForm() {
    }
    
    // MARK: addTapGestureToScrollView
    
    func addTapGestureToScrollView() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    // MARK: Adjusting scrollView size depending on showing/ hiding keyboard
    
    private func addKeyboardNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let keyboardFrameSizeValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
            else {return}
        let keyboardFrameSize = keyboardFrameSizeValue.cgRectValue
        scrollView.contentSize = CGSize(width: view.bounds.size.width, height: scrollView.bounds.size.height + keyboardFrameSize.height)
    }
    
    @objc func keyboardDidHide() {
        scrollView.contentSize = CGSize(width: view.bounds.size.width, height: scrollView.bounds.size.height)
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardNotificationObserver()
        addTapGestureToScrollView()
        scrollView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(statusOfPostSaving), name: NSNotification.Name("DataSavingStatus"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("DataSavingStatus"), object: nil)
    }
    

}
