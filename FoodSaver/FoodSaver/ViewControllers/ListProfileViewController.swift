//
//  ListProfileViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 07/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ListProfileViewController: ListViewController {
    
    // MARK: UI Settings
    
    override func setLocalizedLabels() {
        // to do: 
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
    // MARK: Fetch data to tableView
    
    override func updateTableView(with posts: [Post]) {
        if self.posts == posts {
            return
        } else {
            let filteredPosts = posts.filter {
                return $0.userID == Auth.auth().currentUser?.uid
            }
            self.posts = filteredPosts.sorted {
                $0.creationDate > $1.creationDate
            }
        }
        tableView.reloadData()
    }
    
    // MARK: tableView Edit Post Status Action
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var post = self.posts[indexPath.row]
        let editAction = UITableViewRowAction(style: .normal, title: NSLocalizedString("Change status", comment: ""), handler: { (action, indexPath) in
            let alert = UIAlertController(title: "", message: "\(NSLocalizedString("Current status of post is", comment: "")) \(post.status.description)\(NSLocalizedString(". If you want to change it please choose:", comment: ""))", preferredStyle: .alert)
            let statuses = Status.allCases.filter {
                $0 != post.status
            }
            alert.addAction(UIAlertAction(title: statuses.first?.description, style: .default, handler: { (updateAction) in
                guard let status = statuses.first else {return}
                post.status = status
                Database.database().reference().child("posts").child(post.postID).setValue(["status": post.status])
                ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Thank you", comment: ""), message: NSLocalizedString("Status of your post updated!", comment: ""), on: self)
            }))
            alert.addAction(UIAlertAction(title: statuses.last?.description, style: .default, handler: { (updateAction) in
                guard let status = statuses.last else {return}
                post.status = status
                Database.database().reference().child("posts").child(post.postID).setValue(["status": post.status])
                ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Thank you", comment: ""), message: NSLocalizedString("Status of your post updated!", comment: ""), on: self)
            }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
            self.present(alert, animated: false)
        })
        return [editAction]
    }
    
    
    
    
}
