//
//  TableViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 19/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

protocol SortTableViewProtocol{
    func tableView(_ parametr: SortingBy)
}

class TableViewController: UITableViewController {
    
    var delegate: SortTableViewProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let index = SortingBy(rawValue: indexPath.row) else {return}
        dismiss(animated: true, completion: nil)
        delegate?.tableView(index)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "sortCellID", for: indexPath) as? SortTableViewCell else {
            fatalError("Can't find cell with id: sortCellID")
        }
        cell.update(index: indexPath.row)
        return cell
    }
    
}
