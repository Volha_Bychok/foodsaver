//
//  DetailedPostViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 03/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseUI
import MessageUI

class DetailedPostViewController: UIViewController {
    
    // MARK: Properties
    
    var post: Post?
    private var user: UserProfile?
    
    // MARK: Outlets
    
    @IBOutlet weak var profileImage: RoundedImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var expirationDateLabel: UILabel!
    @IBOutlet weak var sendEmailButton: UIButton!
    @IBOutlet weak var telegramButton: UIButton!
    @IBOutlet weak var viberButton: UIButton!
    @IBOutlet weak var contactMeLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialValues()
    }
    
    // MARK: Contact me buttons
    
    @IBAction func contactViaViberButton(_ sender: UIButton) {
        guard let user = user,
            let phoneNumber = user.phoneNumber,
            let url = URL(string: "viber://contact?number=\(phoneNumber)")
            else {return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    @IBAction func contactViaTelegramButton(_ sender: UIButton) {
        guard let user = user,
            let phoneNumber = user.phoneNumber,
            let url = URL(string: "tg://msg?to=\(phoneNumber)")
            else {return}
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    // MARK: update labels text
    
    private func setInitialValues() {
        contactMeLabel.text = NSLocalizedString("contact me", comment: "")
        guard let post = post else {return}
        let userReference = Storage.storage().reference().child("userProfilePhotos").child(post.userID)
        profileImage.sd_setImage(with: userReference, placeholderImage: UIImage(named: "profileImage"))
        descriptionLabel.text = post.description
        let reference = Storage.storage().reference().child("postPhotos").child(post.postID)
        itemImageView.sd_setImage(with: reference, placeholderImage: UIImage(named: "placeholder"))
        addressLabel.text = post.address.title
        timer()
        FetchDataFromDatabase.shared.fetchingUserData(ID: post.userID, andUse: updateUserInfo(user:))
    }
    
    private func updateUserInfo(user: UserProfile) {
        self.user = user
        nameLabel.text = user.nickname
        sendEmailButton.isUserInteractionEnabled = !(user.email == nil)
        telegramButton.isUserInteractionEnabled = user.telegram
        viberButton.isUserInteractionEnabled = user.viber
        ratingLabel.text = "\(user.rating)"
    }
    
    // MARK: calculating time for post expiring
    
    private func timer() {
        guard let post = post else {return}
        let today = Date()
        let time = Calendar.current.dateComponents([Calendar.Component.hour, Calendar.Component.day], from: today, to: post.expirationDate)
        guard let days = time.day,
            let hours = time.hour
            else {return}
        if days >= 1 {
            expirationDateLabel.text = "\(days) \(NSLocalizedString("days", comment: ""))"
        } else {
            expirationDateLabel.text = "\(hours) \(NSLocalizedString("hours", comment: ""))"
        }
    }
    
}

// MARK: DetailedPostViewController

extension DetailedPostViewController: MFMailComposeViewControllerDelegate {
    
    @IBAction func sendEmailButton(_ sender: UIButton) {
        sendEmail()
    }
    
    func sendEmail() {
        guard let user = user,
            let email = user.email
            else {return}
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("", isHTML: true)
            present(mail, animated: true)
        } else {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: NSLocalizedString("You can send email from device", comment: ""), on: self)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}


