//
//  ProfileViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/16/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import GoogleSignIn
import FirebaseDatabase
import FirebaseAuth
import FirebaseUI
import SDWebImage
import MessageUI

class ProfileViewController: UIViewController {
    
    // Proprties
    
    var user: UserProfile?
    
    // MARK: - Outlets
    
    @IBOutlet weak var profileImage: RoundedImageView!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingTextLabel: UILabel!
    
    @IBOutlet weak var editProfileData: UIButton!
    @IBOutlet weak var activityHistoryButton: UIButton!
    @IBOutlet weak var likedItemsButton: UIButton!
    @IBOutlet weak var rulesOfServiceButton: UIButton!
    @IBOutlet weak var notificationsLabel: UILabel!
    @IBOutlet weak var helpButton: UIButton!
    
    
    // MARK: LogOut
    
    @IBAction func logOutButton(_ sender: RoundedButton) {
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        GIDSignIn.sharedInstance().signOut()
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        Navigation.shared.goToLoginPage()
    }
    
    // MARK: Set initial profile data
    
    private func fetchingUserData() {
        guard let id = Auth.auth().currentUser?.uid else {return}
        FetchDataFromDatabase.shared.fetchingUserData(ID: id, andUse: updateUserInfo(user:))
    }
    
    func updateUserInfo(user: UserProfile) {
        self.user = user
        let reference = Storage.storage().reference().child("userProfilePhotos").child(user.id)
        profileImage.sd_setImage(with: reference,
        maxImageSize: 10000000,
        placeholderImage: UIImage(named: "profileImage"),
        options: .fromLoaderOnly,
        completion: nil)
        nicknameLabel.text = user.nickname
        ratingLabel.text = "\(user.rating)"
    }
    
    // MARK: Set initial text data
    
    private func setTextData() {
        ratingTextLabel.text = L.o.c("rating")
        editProfileData.setTitle(L.o.c("edit profile"), for: .normal)
        activityHistoryButton.setTitle(L.o.c("activity history"), for: .normal)
        likedItemsButton.setTitle(L.o.c("liked items"), for: .normal)
        rulesOfServiceButton.setTitle(L.o.c("rules of service"), for: .normal)
        notificationsLabel.text = L.o.c("notifications")
        helpButton.setTitle(L.o.c("help&feedback"), for: .normal)
    }
    
    // MARK: setNavigationBarSettingsu
    
    private func setNavigationBarSettings() {
        guard let lightBarStyle = UIBarStyle(rawValue: 1) else {return}
        navigationController?.navigationBar.barStyle = lightBarStyle
        navigationController?.navigationBar.barTintColor = UIColor(red:0.13, green:0.38, blue:0.54, alpha:1.0)
    }
    
    // MARK: Seque to edit profile
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditProfileSegue" {
            if let viewController = segue.destination as? EditProfileViewController {
                viewController.delegate = self
                viewController.currentUser = user
            }
        }
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchingUserData()
        setNavigationBarSettings()
        setTextData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !UserDefaults.standard.bool(forKey: "isLoggedIn") {
            ProjectAlerts.shared.presentLoginAlert(page: self)
        }
        self.navigationController?.navigationBar.isHidden = true
    }
    
}

extension ProfileViewController: MFMailComposeViewControllerDelegate {
    
    @IBAction func sendEmailTapped(_ sender: UIButton) {
        sendEmail()
    }
    
    private func sendEmail() {
        let email = "volhabychok@gmail.com"
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("", isHTML: true)
            present(mail, animated: true)
        } else {
            ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: NSLocalizedString("You can't send email from device", comment: ""), on: self)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
