//
//  ListViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/16/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import FirebaseDatabase
import CoreLocation
import FirebaseAuth

class ListViewController: UIViewController {
    
    // MARK: Properties
    
    var posts = [Post]()
    private let locationManager = CLLocationManager()
    private var currentUserLocation: Location?
    private let segueID = "OpenDetailedPost"
    private let listRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        return refreshControl
    } ()
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sortButton: UIButton!
    
    // MARK: Sort tableView
    
    private func openSortMenu() {
        guard let popVC = storyboard?.instantiateViewController(withIdentifier: "popVC") as? TableViewController else {return}
        popVC.modalPresentationStyle = .popover
        let popOverVC = popVC.popoverPresentationController
        popVC.delegate = self
        popOverVC?.delegate = self
        popOverVC?.sourceView = self.sortButton
        popOverVC?.sourceRect = CGRect(x: self.sortButton.bounds.maxX + 35, y: self.sortButton.bounds.maxY, width: 0, height: 0)
        popVC.preferredContentSize = CGSize(width: 200, height: 120)
        self.present(popVC, animated: true)
    }
    
    @IBAction func sortButton(_ sender: UIButton) {
        openSortMenu()
    }
    
    // MARK: Fetch data to tableView
    
    func updateTableView(with posts: [Post]) {
        var filteredPosts: [Post]
        if self.posts == posts {
            return
        } else {
            filteredPosts = posts.filter {
                return $0.status != .broned && $0.userID != Auth.auth().currentUser?.uid
            }
            self.posts = filteredPosts.sorted {
                $0.creationDate > $1.creationDate
            }
        }
        tableView.reloadData()
    }
    
    @objc private func refresh(sender: UIRefreshControl) {
        FetchDataFromDatabase.shared.fetchDataFromDatabase(andUseFor: updateTableView)
        sender.endRefreshing()
    }
    
    //MARK: UI Settings
    
    private func setNavigationBarSettings() {
        guard let lightBarStyle = UIBarStyle(rawValue: 1) else {return}
        navigationController?.navigationBar.barStyle = lightBarStyle
        navigationController?.navigationBar.barTintColor = UIColor(red:0.13, green:0.38, blue:0.54, alpha:1.0)
    }
    
    func setLocalizedLabels() {
        sortButton.setTitle(NSLocalizedString("sort list by", comment: "sort list by"), for: .normal)
    }
    
    // MARK: Life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.refreshControl = listRefreshControl
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalizedLabels()
        enableBasicLocationServise()
        setNavigationBarSettings()
        FetchDataFromDatabase.shared.fetchDataFromDatabase(andUseFor: updateTableView)
    }
    
}

// MARK: UITableViewDataSource

extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as? ListTableViewCell else {
            fatalError("Can't find cell with id: CellID")
        }
        cell.post = posts[indexPath.row]
        cell.updateCell()
        return cell
    }
}

//MARK: UITableViewDelegate

extension ListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let detailVC = storyboard.instantiateViewController(withIdentifier: "detailVC")
            as? DetailedPostViewController else { return }
        detailVC.post = posts[indexPath.row]
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}

// MARK: UIPopoverPresentationControllerDelegate

extension ListViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

// MARK: CLLocationManagerDelegate

extension ListViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined: return
        case .restricted, .denied: return
        case .authorizedWhenInUse, .authorizedAlways:
            locationManager.startUpdatingLocation()
            return
        default:
            print("unknown default")
        }
    }
    
    private func enableBasicLocationServise() {
        locationManager.delegate = self
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return
        case .restricted, .denied:
            return
        case .authorizedWhenInUse, .authorizedAlways:
            return
        default:
            print("unknown default")
        }
    }
    
}

// MARK: SortTableViewProtocol

extension ListViewController: SortTableViewProtocol {
    
    func tableView(_ parametr: SortingBy) {
        posts = parametr.sortTableView(on: self)
        tableView.reloadData()
    }
    
}
