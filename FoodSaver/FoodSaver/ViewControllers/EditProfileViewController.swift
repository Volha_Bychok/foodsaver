//
//  EditProfileViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 05/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import FirebaseUI
import FirebaseDatabase
import SDWebImage
import FirebaseAuth

class EditProfileViewController: ScrollViewController {
    
    // MARK Properties
    
    private let id = Auth.auth().currentUser?.uid
    var currentUser: UserProfile?
    var updatedUser: UserProfile?
    weak var delegate: ProfileViewController?
    
    // MARK: Outlets
    
    @IBOutlet weak var imageView: RoundedImageView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Buttons
    
    @IBAction func changePhotoTapped(_ sender: UIButton) {
        presentImagePickerController(sourceType: .photoLibrary)
    }
    
    @IBAction func saveButtonTapped(_ sender: UIBarButtonItem) {
        dismissKeyboard()
        if currentUser == updatedUser {
            print("nothing changed")
        } else {
            guard let updatedUser = updatedUser else {return}
            updatedUser.saveUserProfileToDatabase()
            ProjectAlerts.shared.presentPleaseWaitAlert(on: self)
            delegate?.updateUserInfo(user: updatedUser)
        }
    }
    
    // Filling in tableView
    
    func updateUserInfo() {
        guard let user = currentUser else {return}
        tableView.reloadData()
        let reference = Storage.storage().reference().child("userProfilePhotos").child(user.id)
        imageView.sd_setImage(with: reference,
                              maxImageSize: 10000000,
                              placeholderImage: UIImage(named: "profileImage"),
                              options: .fromLoaderOnly,
                              completion: nil)
    }
    
    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        updatedUser = currentUser
        updateUserInfo()
        self.navigationController?.navigationBar.isHidden = false
    }
    
}

// MARK: UITableViewDataSource

extension EditProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row <= 2 || indexPath.row == 5 {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EditProfileDataCell", for: indexPath) as? EditProfileDataTableViewCell else {fatalError("Can't find cell with id: CellID")}
            cell.delegate = self
            cell.index = indexPath.row
            cell.updateLabels()
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessengerCheckboxCell", for: indexPath) as? MessengerCheckboxTableViewCell else {fatalError("Can't find cell with id: CellID")}
            cell.index = indexPath.row
            cell.delegate = self
            cell.updateTextLabel()
            return cell
        }
    }
    
}

// MARK: UIImagePickerControllerDelegate

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private func presentImagePickerController(sourceType: UIImagePickerController.SourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = sourceType
        present(imagePickerController, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            imageView.image = image
            updatedUser?.photo = image
        }
        dismiss(animated: true, completion: nil)
    }
    
}
