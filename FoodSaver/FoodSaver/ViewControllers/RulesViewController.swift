//
//  RulesViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 09/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class RulesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = NSLocalizedString("Rules of service", comment: "")
    }

}
