//
//  LikedListProfileViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 08/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import FirebaseAuth

class LikedListProfileViewController: ListProfileViewController {
    
    // MARK: Filter posts in tableView
    
    override func updateTableView(with posts: [Post]) {
        var likedPosts = [Post]()
        if self.posts == posts {
            return
        } else {
            for post in posts {
                for (_, value) in post.likedBy {
                    if value == Auth.auth().currentUser?.uid {
                        likedPosts.append(post)
                    }
                }
            }
            self.posts = likedPosts
        }
        tableView.reloadData()
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
