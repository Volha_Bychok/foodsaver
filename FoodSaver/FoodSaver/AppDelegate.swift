//
//  AppDelegate.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/14/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        guard let vc = GIDSignIn.sharedInstance()?.presentingViewController else {return}
        if let error = error {
            print(error)
            return
        }
        guard let fullName = user?.profile.name,
            let image = user?.profile.imageURL(withDimension: 400),
            let email = user?.profile.email
            else {return}
        guard let authentication = user?.authentication else {return}
        let credential = GoogleAuthProvider.credential(withIDToken: user.authentication.idToken, accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential, completion: { (result, error) in
            if error == nil {
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                Navigation.shared.goToList()
                guard let newUser = result?.additionalUserInfo?.isNewUser else {return}
                if newUser {
                    guard let id = Auth.auth().currentUser?.uid else {return}
                    let user = UserProfile(id: id, email: email, nickname: fullName, photoURL: image)
                    user.saveUserProfileToDatabase()
                }
            } else {
                ProjectAlerts.shared.presentAlert(title: "Something went wrong", message: String(describing: error), on: vc)
            }
        })
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
    }
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        ApplicationDelegate.shared.application(app, open: url, options: options)
        return GIDSignIn.sharedInstance().handle(url)
    }
}

