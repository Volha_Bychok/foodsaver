//
//  SortTableViewCell.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 13/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class SortTableViewCell: UITableViewCell {

    func update(index: Int){
        textLabel?.textColor = UIColor(red:0.13, green:0.38, blue:0.54, alpha:1.0)
        switch index {
        case 0:
            textLabel?.text =  NSLocalizedString("distance", comment: "")
            imageView?.image = UIImage(named: "sortDown")
        case 1:
            textLabel?.text = NSLocalizedString("date", comment: "")
            imageView?.image = UIImage(named: "sortDown")
        default:
            textLabel?.text = NSLocalizedString("rating", comment: "")
            imageView?.image = UIImage(named: "sortUp")
        }
    }

}
