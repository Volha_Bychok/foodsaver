//
//  ListTableViewCell.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/16/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseStorage
import FirebaseUI
import FirebaseDatabase
import FirebaseAuth

class ListTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    var post: Post?
    private var positiveRating: Bool?
    
    // MARK: Outlets
    
    @IBOutlet weak var itemInfoLabel: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemView: UIStackView!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    
    // MARK: Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let view = UIView()
        view.backgroundColor = .clear
        selectedBackgroundView = view
    }
    
    // MARK: Methods
    
    private func addRatingToUser(user: UserProfile) {
        guard let positiveRating = positiveRating else {return}
        var rating = user.rating
        if positiveRating {
            rating += 1
        } else {
            rating -= 1
        }
        let user = UserProfile(id: user.id, email: user.email, nickname: user.nickname, photo: nil, rating: rating, address: user.address, phoneNumber: user.phoneNumber, viber: user.viber, telegram: user.telegram)
        user.saveUserProfileToDatabase()
    }
    
    @IBAction func likeButtonTapped(_ sender: UIButton) {
        guard let currentUser = Auth.auth().currentUser else {return}
        if sender.isSelected {
            post?.rating -= 1
            positiveRating = false
            post?.findUserWhoUnliked(userID: currentUser.uid)
        } else {
            post?.rating += 1
            positiveRating = true
            var likedBy = post?.likedBy ?? [" ":" "]
            likedBy[String(post?.rating ?? 0)] = currentUser.uid
            post?.likedBy = likedBy
        }
        post?.savePostToDatabase()
        if post != nil {
            guard let postRating = post?.rating
                else {return}
            FetchDataFromDatabase.shared.fetchingUserData(ID: post!.userID, andUse: addRatingToUser)
            likesLabel.text = "\(postRating) \(NSLocalizedString("likes", comment: ""))"
            sender.isSelected = !sender.isSelected
        }
    }
    
    func updateCell() {
        likeButton.isSelected = false
        guard let post = post,
            let distance = post.distance
            else {return}
        destinationLabel.text = "\(String(((distance) / 1000.0).rounded())) \(NSLocalizedString("km", comment: ""))"
        itemInfoLabel.text = post.description
        likesLabel.text = "\(post.rating) \(NSLocalizedString("likes", comment: ""))"
        let reference = Storage.storage().reference().child("postPhotos").child(post.postID)
        itemImage.sd_setImage(with: reference, placeholderImage: UIImage(named: "placeholder"))
        for (_, value) in post.likedBy {
            if value == Auth.auth().currentUser?.uid {
                likeButton.isSelected = true
            }
        }
    }
    
}
