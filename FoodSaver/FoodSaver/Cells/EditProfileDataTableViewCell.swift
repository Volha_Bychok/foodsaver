//
//  EditProfileDataTableViewCell.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 05/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class EditProfileDataTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    weak var delegate: EditProfileViewController?
    var index: Int?
    
    // MARK: Outlets
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellTextField: UITextField!
    
    // MARK: Methods
    
    @IBAction func cellTextField(_ sender: UITextField) {
        switch index {
        case 0:
            delegate?.updatedUser?.nickname = cellTextField.text ?? " "
        case 1:
            delegate?.updatedUser?.email = cellTextField.text
        case 2:
            delegate?.updatedUser?.address = cellTextField.text
        default:
            delegate?.updatedUser?.phoneNumber = cellTextField.text
        }
    }
    
    
    func updateLabels() {
        switch index {
        case 0:
            cellLabel.text = NSLocalizedString("nickname", comment: "")
            cellTextField.text = delegate?.updatedUser?.nickname
        case 1:
            cellLabel.text = NSLocalizedString("email", comment: "")
            cellTextField.text = delegate?.updatedUser?.email
        case 2:
            cellLabel.text = NSLocalizedString("address", comment: "")
            cellTextField.text = delegate?.updatedUser?.address
        default:
            cellLabel.text = NSLocalizedString("phone number", comment: "")
            cellTextField.text = delegate?.updatedUser?.phoneNumber
        }
    }
    

}
