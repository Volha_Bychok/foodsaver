//
//  MessengerCheckboxTableViewCell.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 05/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit

class MessengerCheckboxTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    weak var delegate: EditProfileViewController?
    var index: Int?
    
    // MARK: Outlets
    
    @IBOutlet weak var checkboxTextLabel: UILabel!
    @IBOutlet weak var checkboxButton: UIButton!
    
    // MARK: Methods
    
    @IBAction func checkboxButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if index == 3 {
            delegate?.updatedUser?.viber = sender.isSelected
        } else {
            delegate?.updatedUser?.telegram = sender.isSelected
        }
    }
    
    func updateTextLabel() {
        guard let user = delegate?.updatedUser else {return}
        if index == 3 {
            checkboxTextLabel.text = NSLocalizedString("do you have telegram account to keep in touch with other users?", comment: "")
            checkboxButton.isSelected = user.viber
        } else {
            checkboxTextLabel.text = NSLocalizedString("do you have viber account to keep in touch with other users?", comment: "")
            checkboxButton.isSelected = user.telegram
        }
    }
    
    
}
