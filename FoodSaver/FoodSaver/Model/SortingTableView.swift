//
//  SortingTableView.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 04/11/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import UIKit

enum SortingBy: Int {
    case distance
    case term
    case rating
    
    
    func sortTableView(on vc: ListViewController) -> [Post] {
        switch self {
        case .distance:
            return vc.posts.sorted(by: {
                $0.distance ?? Double.greatestFiniteMagnitude < $1.distance ?? Double.greatestFiniteMagnitude
            })
        case .rating:
            return vc.posts.sorted(by: {
                $0.rating > $1.rating
            })
        case .term:
            return vc.posts.sorted(by: {
                $0.creationDate > $1.creationDate
            })
        }
    }
    
}


