//
//  UserProfile.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 16/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import UIKit

struct UserProfile: Equatable {
    
    var id: String
    var email: String?
    var nickname: String
    var photo: UIImage?
    var photoURL: URL?
    var rating: Int
    var address: String?
    var phoneNumber: String?
    var viber: Bool
    var telegram: Bool
    
    func saveUserProfileToDatabase() {
        var image: UIImage?
        if photo == nil && photoURL != nil {
            URLSession.shared.dataTask(with: photoURL!) { (data, _, error) in
                guard let data = data, error == nil else {
                    return
                }
                image = UIImage(data: data)
                SavingData.shared.saveImageToFolder("userProfilePhotos", image: image, id: self.id)
                }.resume()
        } else {
        SavingData.shared.saveImageToFolder("userProfilePhotos", image: photo, id: id)
        }
        
        let dictionary = transformUserProfileToDictionary()
        SavingData.shared.saveData(dictionary: dictionary, ID: id, folder: "users")
    }
    
    private func transformUserProfileToDictionary() -> [String: Any] {
        let dictionary: [String: Any] = ["email": email as Any, "nickname": nickname, "raiting": rating, "address": address as Any, "phoneNumber": phoneNumber as Any, "viber": viber, "telegram": telegram]
        return dictionary
    }
    
    init(id: String, dictionary: [String: Any]) {
        self.id = id
        self.email = dictionary["email"] as? String
        self.nickname = (dictionary["nickname"] as? String) ?? " "
        self.rating = dictionary["raiting"] as? Int ?? 0
        self.address = dictionary["address"] as? String
        self.phoneNumber = dictionary["phoneNumber"] as? String
        self.viber = dictionary["viber"] as? Bool ?? false
        self.telegram = dictionary["telegram"] as? Bool ?? false
    }
    
    init(id: String, email: String?, nickname: String, photo: UIImage?, rating: Int, address: String?, phoneNumber: String?, viber: Bool, telegram: Bool){
        self.id = id
        self.email = email
        self.nickname = nickname
        self.photo = photo
        self.rating = rating
        self.address = address
        self.phoneNumber = phoneNumber
        self.viber = viber
        self.telegram = telegram
    }
    
    init(id: String, email: String?, nickname: String, photoURL: URL?) {
        self.id = id
        self.email = email
        self.nickname = nickname
        self.photoURL = photoURL
        self.rating = 0
        self.address = nil
        self.phoneNumber = nil
        self.viber = false
        self.telegram = false
    }
}
