//
//  Location.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 30/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import CoreLocation

struct Location: Equatable {
    
    let title: String
    let coordinates: CLLocation
    
}
