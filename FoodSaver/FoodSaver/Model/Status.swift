//
//  Status.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 30/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation

enum Status: Int, CaseIterable, CustomStringConvertible {
    
    var description: String {
        switch self {
        case .active: return NSLocalizedString("active", comment: "active")
        case .deactive: return NSLocalizedString("deactive", comment: "deactive")
        case .broned: return NSLocalizedString("broned", comment: "broned")
        }
    }
    
    case deactive
    case broned
    case active
}
