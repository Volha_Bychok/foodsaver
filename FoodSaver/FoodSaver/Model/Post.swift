//
//  Post.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 30/10/2019.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct Post: Equatable {
    
    let creationDate: Date
    let locationManager = CLLocationManager()
    let postID: String
    let userID: String
    var photo: UIImage?
    var description: String
    let address: Location
    let expirationDate: Date
    var rating: Int
    var status: Status
    let distance: Double?
    var likedBy: [String: String]
    
    mutating func findUserWhoUnliked(userID: String) {
        let userWhoUnlikedKey = likedBy.filter {$0.value == userID}.first?.key
        guard let key = userWhoUnlikedKey else {return}
        self.likedBy.removeValue(forKey: key)
    }
    
    func savePostToDatabase() {
        SavingData.shared.saveImageToFolder("postPhotos", image: photo, id: postID)
        let dictionary = transformPostToDictionary()
        SavingData.shared.saveData(dictionary: dictionary, ID: postID, folder: "posts")
    }
    
    private func transformPostToDictionary() -> [String: Any] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH-mm-ss"
        let expirationDateString = dateFormatter.string(from: expirationDate)
        let creationDateString = dateFormatter.string(from: creationDate)
        let dictionary: [String: Any] = ["postID": postID, "userID": userID, "description": description, "address": address.title, "lat": address.coordinates.coordinate.latitude, "lon": address.coordinates.coordinate.longitude, "expirationDate": expirationDateString, "rating": rating, "status": status.rawValue, "creationDate": creationDateString, "likedBy": likedBy]
        return dictionary
    }
    
    init?(dictionary: [String: Any]) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH-mm-ss"
        guard let dateString = dictionary["expirationDate"] as? String,
            let post = dictionary["postID"] as? String,
            let user = dictionary["userID"] as? String,
            let rating = dictionary["rating"] as? Int,
            let statusValue = dictionary["status"] as? Int,
            let status = Status(rawValue: statusValue),
            let date = dateFormatter.date(from: dateString),
            let address = dictionary["address"] as? String,
            let lat = dictionary["lat"] as? Double,
            let lon = dictionary["lon"] as? Double,
            let description = dictionary["description"] as? String,
            let creationDateString = dictionary["creationDate"] as? String,
            let creationDate = dateFormatter.date(from: creationDateString)
            else {return nil}
        self.expirationDate = date
        let coordinates = CLLocation(latitude: lat, longitude: lon)
        self.address = Location(title: address, coordinates: coordinates)
        self.description = description
        self.postID = post
        self.userID = user
        self.rating = rating
        self.status = status
        self.distance = locationManager.location?.distance(from: coordinates)
        self.likedBy = dictionary["likedBy"] as? [String : String] ?? [" ": " "]
        self.creationDate = creationDate
    }
    
    init?(postID: String, userID: String, photo: UIImage?, description: String, address: Location, expirationDate: Date, rating: Int, status: Status, likedBy: [String: String], creationDate: Date) {
        self.description = description
        self.postID = postID
        self.userID = userID
        self.photo = photo
        self.address = address
        self.expirationDate = expirationDate
        self.rating = rating
        self.status = status
        self.distance = nil
        self.likedBy = likedBy
        self.creationDate = creationDate
    }
    
}
