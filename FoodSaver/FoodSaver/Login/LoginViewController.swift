//
//  LoginViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/15/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn
import FirebaseDatabase

class LoginViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: RoundedButton!
    @IBOutlet weak var signUpButton: RoundedButton!
    @IBOutlet weak var nickNameTextField: UITextField!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var orUseLabel: UILabel!
    
    
    // MARK: - Exit
    
    @IBAction func exitButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Social network login buttons
    
    @IBAction func facebookLoginButton(_ sender: UIButton) {
        Login.shared.facebookLogin(on: self)
    }
    
    @IBAction func googleLoginButton(sender: AnyObject) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func twitterLoginButton(_ sender: UIButton) {
        Login.shared.twitterLogin(on: self)
    }
    
    //MARK: - Email/ password login/ sign up
    
    @IBAction func loginButton(_ sender: RoundedButton) {
        if sender.isSelected {
            Login.shared.loginWithEmail(on: self, with: emailTextField.text, password: passwordTextField.text)
        } else {
            configurePageState(login: true)
        }
    }
    
    @IBAction func signUpButton(_ sender: RoundedButton) {
        if sender.isSelected {
            Login.shared.signUp(on: self, with: emailTextField.text, password: passwordTextField.text, nickname: nickNameTextField.text)
        } else {
            configurePageState(login: false)
        }
    }
    
    //MARK: - Methods
    
    private func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            passwordTextField.resignFirstResponder()
            Login.shared.loginWithEmail(on: self, with: emailTextField.text, password: passwordTextField.text)
        }
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    // MARK: - Login/ Sign Up state of page
    
    func configurePageState(login: Bool) {
        loginButton.isSelected = login
        signUpButton.isSelected = !login
        nickNameTextField.isHidden = login
    }
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurePageState(login: true)
        orUseLabel.text = L.o.c("OR USE")
        let attributedString = NSMutableAttributedString.init(string: L.o.c("Forgot password?"))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSRange.init(location: 0, length: attributedString.length))
        forgotPasswordButton.setAttributedTitle(attributedString, for: .normal)
        forgotPasswordButton.titleLabel?.textColor = .white
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        if UserDefaults.standard.bool(forKey: "isLoggedIn") {
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
}
