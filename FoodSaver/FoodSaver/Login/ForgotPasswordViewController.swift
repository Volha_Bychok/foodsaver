//
//  ForgotPasswordViewController.swift
//  FoodSaver
//
//  Created by Ольга Бычок on 9/16/19.
//  Copyright © 2019 Ольга Бычок. All rights reserved.
//

import UIKit
import FirebaseAuth

class ForgotPasswordViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    
    // MARK: - Methods
    
    @IBAction func sendMeLinkButton(_ sender: UIButton) {
        guard let email = emailTextField.text else {return}
        Auth.auth().sendPasswordReset(withEmail: email) { [weak self] (error) in
            guard let self = self else {return}
            if error == nil {
                ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Link is sent", comment: "Link is sent"), message: NSLocalizedString("We sent you an email with a link to get back into your account", comment: ""), on: self)
            } else {
                ProjectAlerts.shared.presentAlert(title: NSLocalizedString("Something went wrong", comment: ""), message: error!.localizedDescription, on: self)
            }
        }
    }
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoLabel.text = L.o.c("Please enter your email and we'll send you a link to get back into your account.")
        emailTextField.placeholder = L.o.c("enter")
        sendButton.setTitle(L.o.c("Send"), for: .normal)
        }
    }
    

